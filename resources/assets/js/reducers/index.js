import { combineReducers } from 'redux';
import auth from './auth';
import loading from './loading';
import dictionary from './dictionary';

export default combineReducers({
  auth,
  loading,
  dictionary
});
