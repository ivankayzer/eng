import { SET_DICTIONARY_LIST } from '../actions/dictionary';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_DICTIONARY_LIST:
      return Object.assign({}, state, action.data);
    default:
      return state;
  }
};
