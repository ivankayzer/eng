import React from 'react';
import AuthNav from '../../components/AuthNav';
import Footer from '../../components/Footer';
import { Container, Col, Row } from 'react-grid-system';

import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.element.isRequired
};

const AppLayout = ({ children, ...rest }) => {
  return (
    <Container>
      <Row>
        <Col sm={3}>
          <div className="component-wrapper">
            <AuthNav />
          </div>
        </Col>
        <Col sm={9}>
          <div className="component-wrapper">
            {children}
          </div>
        </Col>
      </Row>
      <Footer />
    </Container>
  );
};

AppLayout.propTypes = propTypes;

export default AppLayout;
