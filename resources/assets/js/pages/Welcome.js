import React from 'react';
import {Redirect} from 'react-router-dom';

const Welcome = () => {
  return (
    <Redirect to="/signin" />
  );
};

export default Welcome;
