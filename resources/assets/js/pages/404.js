import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {Col} from 'react-grid-system';
import AppLayoutRoute from '../router/AppLayoutRoute';

const propTypes = {
  authenticated: PropTypes.bool.isRequired
};

const NotFoundComponent = (props) => (
  <Col sm={8}>
    <h1 className="py-8">Sorry, that page isn’t here.</h1>
    <p className="text-grey-dark">
      You didn’t do anything wrong. We may have moved the page you’re looking for somewhere else.
    </p>
  </Col>
);

const NotFound = (props) => {
  return (
    <AppLayoutRoute component={NotFoundComponent} {...props} />
  );
};

NotFound.propTypes = propTypes;
const mapStateToProps = ({ auth: { authenticated } }) => ({ authenticated });
export default connect(mapStateToProps)(NotFound);
