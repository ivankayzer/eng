import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import {Col, Row} from 'react-grid-system';
import { addWord, getList } from '../actions/dictionary';
import DictionaryCard from '../components/DictionaryCard';

const propTypes = {
  auth: PropTypes.object.isRequired,
  addWord: PropTypes.func.isRequired,
  getList: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  dictionary: PropTypes.object
};

class Dictionary extends Component {
  constructor (props) {
    super(props);
    this.state = {
      word: '',
      user: this.props.auth.user
    };
  }

  handleInputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...{ [e.target.name]: '' }
      }
    });
  };

  componentWillMount () {
    this.props.getList();
  }

  addWord = () => {
    this.props.addWord({ text: this.state.word});
  };

  render () {
    return (
      <DocumentTitle title={`Home - ${window.App.name}`}>
        <div>
          <Row justify="between" style={{'padding': '15px'}}>
            <Col sm={5}>
              <div className="bp3-input-group">
                <input type="text" className="bp3-input" name="word" onChange={this.handleInputChange} value={this.state.word} placeholder="Add new word" />
                <button className="bp3-button bp3-minimal bp3-intent-primary bp3-icon-plus" onClick={this.addWord}/>
              </div>
            </Col>
            <Col sm={4}>
              <div className="bp3-input-group">
                <input type="text" className="bp3-input" name="word" onChange={this.handleInputChange} value={this.state.word} placeholder="Search" />
                <button className="bp3-button bp3-minimal bp3-intent-primary bp3-icon-search" onClick={this.addWord}/>
              </div>
            </Col>
          </Row>

          <div style={{'marginTop': '15px'}}>
            <Row>
              { Object.values(this.props.dictionary).map((entry, index) => <DictionaryCard {...entry} key={entry.id} />) }
            </Row>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

const mapDispatchToProps = {
  addWord,
  getList
};

Dictionary.propTypes = propTypes;
const mapStateToProps = ({auth, loading, dictionary}) => ({auth, loading, dictionary});
export default connect(mapStateToProps, mapDispatchToProps)(Dictionary);
