import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import { signInUser, googleSignIn } from '../../actions/auth';
import { getIntendedUrl } from '../../helpers/auth';
import { destructServerErrors, hasError, getError } from '../../helpers/error';
import GoogleSignIn from '../../components/GoogleSignIn';
import {Button, FormGroup, InputGroup, Tag} from '@blueprintjs/core';
import { Col } from 'react-grid-system';

const propTypes = {
  signInUser: PropTypes.func.isRequired,
  googleSignIn: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

class SignIn extends Component {
  constructor (props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errors: ''
    };
  }

  signInSuccess () {
    getIntendedUrl().then(url => this.props.history.push(url));
  }

  handleSubmit (e) {
    e.preventDefault();
    this.props.signInUser(this.state)
      .then(response => this.signInSuccess())
      .catch(error => this.setState({ errors: destructServerErrors(error) }));
  }

  handleInputChange (e) {
    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...{ [e.target.name]: '' }
      }
    });
  }

  handleGoogleSignInSuccess (credentials) {
    this.props.googleSignIn(credentials)
      .then(response => this.signInSuccess())
      .catch(error => this.setState({ errors: destructServerErrors(error) })); ;
  }

  render () {
    return (
      <DocumentTitle title={`Sign in - ${window.App.name}`}>
        <Col sm={8}>
          <h1 className="bp1-heading">Sign in</h1>
          <form onSubmit={e => this.handleSubmit(e)}
            method="POST">
            <FormGroup
              label="Email address"
              labelFor="text-input"
            >
              <InputGroup id="email" name="email" placeholder="contact@gmail.com" value={this.state.email}
                onChange={e => this.handleInputChange(e)} type="email" />
              {hasError(this.state.errors, 'email') &&
                      <p className="text-red text-xs pt-2">{getError(this.state.errors, 'email')}</p>
              }
            </FormGroup>
            <FormGroup
              label="Password"
              labelFor="password"
            >
              <InputGroup id="password" name="password" placeholder="********" type="password" value={this.state.password}
                onChange={e => this.handleInputChange(e)} />
            </FormGroup>

            <Button rightIcon="arrow-right" intent="success" text="Sign In" type="submit" />
          </form>


          <div className="mt">
            <GoogleSignIn googleSignInSuccess={(credentials) => this.handleGoogleSignInSuccess(credentials)} />
          </div>

          <div className="mt">
            <div>
              <span>Create a New Account? </span>
              <Link to="/register" className="no-underline text-grey-darker font-bold">Register</Link>
            </div>

            <div className="mt">
              <Tag>Help</Tag> <Link to="/forgot-password" className="no-underline text-grey-dark text-xs">Reset Password</Link>
            </div>
          </div>
        </Col>
      </DocumentTitle>
    );
  }
}

SignIn.propTypes = propTypes;

const mapDispatchToProps = {
  signInUser,
  googleSignIn
};

export default connect(null, mapDispatchToProps)(withRouter(SignIn));
