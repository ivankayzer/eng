import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import DocumentTitle from 'react-document-title';
import { destructServerErrors, hasError, getError } from '../../helpers/error';

import PropTypes from 'prop-types';
import {Button, Callout, FormGroup, InputGroup, Intent} from '@blueprintjs/core';
import {Col} from 'react-grid-system';

const propTypes = {
  match: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

class ResetPassword extends Component {
  constructor (props) {
    super(props);
    this.state = {
      'email': '',
      'password': '',
      'password_confirmation': '',
      'token': '',
      'errors': '',
      'resetMessage': ''
    };
  }

  componentDidMount () {
    this.setState({
      'token': this.props.match.params.token
    });
  }

  handleSubmit (e) {
    e.preventDefault();
    window.axios.post('/api/password/reset', this.state)
      .then(({ data: { status } }) => {
        this.setState({ 'resetMessage': status });
      }).catch(error => {
        this.setState({ errors: destructServerErrors(error) });
      });
  }

  handleInputChange (e) {
    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...{ [e.target.name]: '' }
      }
    });
  }

  render () {
    return (
      <DocumentTitle title={`Reset password - ${window.App.name}`}>
        <Col sm={8}>
          {this.state.resetMessage !== '' && <Callout intent={Intent.SUCCESS}>{this.state.resetMessage}
            <span>
                   Please
              <Link to="/signin" className="no-underline text-grey-darker font-bold"> login </Link>
                  with your new password
            </span>
          </Callout>
          }

          <form
            onSubmit={e => this.handleSubmit(e)}
            method="POST"
            className="border rounded bg-white border-grey-light w-3/4 sm:w-1/2 lg:w-2/5 xl:w-1/3 px-8 py-4">

            <h2 className="text-center mb-4 text-grey-darker">Reset Your Password</h2>

            <FormGroup
              label="Email address"
              labelFor="text-input"
            >
              <InputGroup id="email" name="email" placeholder="contact@gmail.com" value={this.state.email}
                onChange={e => this.handleInputChange(e)} type="email" />
              {hasError(this.state.errors, 'email') &&
              <p className="text-red text-xs pt-2">{getError(this.state.errors, 'email')}</p>
              }
            </FormGroup>
            <FormGroup
              label="Password"
              labelFor="password"
            >
              <InputGroup id="password" name="password" placeholder="********" type="password" value={this.state.password}
                onChange={e => this.handleInputChange(e)} />
            </FormGroup>
            <FormGroup
              label="Password confirmation"
              labelFor="password_confirmation"
            >
              <InputGroup id="password_confirmation" name="password_confirmation" placeholder="********" type="password" value={this.state.password_confirmation}
                onChange={e => this.handleInputChange(e)} />
            </FormGroup>

            <Button rightIcon="arrow-right" intent="success" text="Reset" type="submit" />
          </form>
        </Col>
      </DocumentTitle>
    );
  }
}

ResetPassword.propTypes = propTypes;

export default ResetPassword;
