import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import DocumentTitle from 'react-document-title';
import {destructServerErrors, hasError, getError} from '../../helpers/error';
import {Col} from 'react-grid-system';
import {Button, Callout, FormGroup, InputGroup, Intent} from '@blueprintjs/core';

class ForgotPassword extends Component {
  constructor (props) {
    super(props);
    this.state = {
      'email': '',
      'errors': '',
      'resetMessage': ''
    };
  }

  handleSubmit (e) {
    e.preventDefault();
    window.axios.post('/api/password/email', {email: this.state.email})
      .then(({data: {status}}) => {
        this.setState({'resetMessage': status});
      }).catch(error => {
        this.setState({errors: destructServerErrors(error)});
      });
  }

  handleInputChange (e) {
    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...{[e.target.name]: ''}
      }
    });
  }

  render () {
    return (
      <DocumentTitle title={`Forgot password - ${window.App.name}`}>
        <Col sm={8}>
          {this.state.resetMessage !== '' && (
            <Callout intent={Intent.SUCCESS}>{this.state.resetMessage}</Callout>
          )
          }
          <form
            onSubmit={e => this.handleSubmit(e)}
            method="POST"
          >
            <h1 className="bp1-heading">Can&#39;t log in?</h1>
            <FormGroup
              label="Email address"
              labelFor="text-input"
            >
              <InputGroup id="email" name="email" placeholder="contact@gmail.com" value={this.state.email}
                onChange={e => this.handleInputChange(e)} type="email"/>
              {hasError(this.state.errors, 'email') &&
              <p className="text-red text-xs pt-2">{getError(this.state.errors, 'email')}</p>
              }
            </FormGroup>
            {hasError(this.state.errors, 'email') &&
            <p className="text-red text-xs pt-2">{getError(this.state.errors, 'email')}</p>
            }

            <Button rightIcon="arrow-right" intent="success" text="Email me reset instructions" type="submit"/>
          </form>

          <div className="mt">
            Never mind,
            <Link
              to="/signin"
              className="text-grey-darkest text-indigo"> go back to the login screen
            </Link>
          </div>
        </Col>
      </DocumentTitle>
    );
  }
}

export default ForgotPassword;
