import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import DocumentTitle from 'react-document-title';
import PropTypes from 'prop-types';
import { registerUser, googleSignIn } from '../../actions/auth';
import { destructServerErrors, hasError, getError } from '../../helpers/error';
import GoogleSignIn from '../../components/GoogleSignIn';
import {Col} from 'react-grid-system';
import {Button, FormGroup, InputGroup, MenuItem} from '@blueprintjs/core';
import { Select } from '@blueprintjs/select';

const propTypes = {
  registerUser: PropTypes.func.isRequired,
  googleSignIn: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

const Languages = [
  {id: 'ru', name: 'Russian'},
  {id: 'pl', name: 'Polish'},
  {id: 'de', name: 'German'},
  {id: 'fr', name: 'French'},
  {id: 'es', name: 'Spanish'},
  {id: 'it', name: 'Italian'}
];

class Register extends Component {
  constructor (props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: '',
      password_confirmation: '',
      language: {
        'id': 'ru',
        'name': 'Russian'
      },
      errors: ''
    };
  }

  filterLanguage = (query, language) => {
    return language.name.toLowerCase().indexOf(query.toLowerCase()) >= 0;
  }

  selectLanguage = (language) => {
    this.setState({ language });
  }

  renderLanguageSelectItem = (language, { handleClick, modifiers, query }) => {
    if (!modifiers.matchesPredicate) {
      return null;
    }

    return (
      <MenuItem
        active={modifiers.active}
        disabled={modifiers.disabled}
        label={language.id}
        key={language.id}
        onClick={handleClick}
        text={language.name}
      />
    );
  }

  registerSuccess () {
    this.props.history.push('/dictionary');
  }

  handleSubmit (e) {
    e.preventDefault();
    this.props.registerUser(this.state)
      .then(response => this.registerSuccess())
      .catch(error => this.setState({ errors: destructServerErrors(error) }));
  }

  handleInputChange (e) {
    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...{ [e.target.name]: '' }
      }
    });
  }

  handleGoogleSignInSuccess (credentials) {
    this.props.googleSignIn(credentials)
      .then(response => this.registerSuccess())
      .catch(error => this.setState({ errors: destructServerErrors(error) })); ;
  }

  render () {
    return (
      <DocumentTitle title={`Register - ${window.App.name}`}>
        <Col sm={8}>
          <h1 className="bp1-heading">Register</h1>
          <form onSubmit={e => this.handleSubmit(e)}
            method="POST"
          >
            <FormGroup
              label="Username"
              labelFor="text-input"
            >
              <InputGroup id="username" name="name" placeholder="johnsmith" value={this.state.name}
                onChange={e => this.handleInputChange(e)} type="text" />
              {hasError(this.state.errors, 'name') &&
              <p className="text-red text-xs pt-2">{getError(this.state.errors, 'name')}</p>
              }
            </FormGroup>
            <FormGroup
              label="Email address"
              labelFor="text-input"
            >
              <InputGroup id="email" name="email" placeholder="contact@gmail.com" value={this.state.email}
                onChange={e => this.handleInputChange(e)} type="email" />
              {hasError(this.state.errors, 'email') &&
              <p className="text-red text-xs pt-2">{getError(this.state.errors, 'email')}</p>
              }
            </FormGroup>
            <FormGroup
              label="Dictionary language"
            >
              <Select
                items={Languages}
                itemPredicate={this.filterLanguage}
                itemRenderer={this.renderLanguageSelectItem}
                onItemSelect={this.selectLanguage}
                noResults={<MenuItem disabled={true} text="No results." />}
              >
                <Button text={this.state.language.name} rightIcon="double-caret-vertical" />
              </Select>
            </FormGroup>
            <FormGroup
              label="Password"
              labelFor="password"
            >
              <InputGroup id="password" name="password" placeholder="********" type="password" value={this.state.password}
                onChange={e => this.handleInputChange(e)} />
            </FormGroup>
            <FormGroup
              label="Password confirmation"
              labelFor="password_confirmation"
            >
              <InputGroup id="password_confirmation" name="password_confirmation" placeholder="********" type="password" value={this.state.password_confirmation}
                onChange={e => this.handleInputChange(e)} />
            </FormGroup>

            <Button rightIcon="arrow-right" intent="success" text="Register" type="submit" />
          </form>
          <div className="mt">
            <GoogleSignIn googleSignInSuccess={(credentials) => this.handleGoogleSignInSuccess(credentials)} />
          </div>
          <div className="mt">
            <span>Already have an account? </span>
            <Link to="/signin" className="no-underline text-grey-darker font-bold"> Sign in</Link>
          </div>
        </Col>
      </DocumentTitle>
    );
  }
}

Register.propTypes = propTypes;

const mapDispatchToProps = { registerUser, googleSignIn };

export default connect(null, mapDispatchToProps)(withRouter(Register));
