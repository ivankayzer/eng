import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Welcome from '../pages/Welcome';
import SignIn from '../pages/auth/SignIn';
import Register from '../pages/auth/Register';
import ForgotPassword from '../pages/auth/ForgotPassword';
import ResetPassword from '../pages/auth/ResetPassword';
import NotFound from '../pages/404';
import PropTypes from 'prop-types';
import Profile from '../pages/Profile';
import AuthRoute from './AuthRoute';
import { connect } from 'react-redux';
import { setLoading } from '../actions/loading';
import { initAuthFromExistingToken } from '../actions/auth';
import GuestRoute from './GuestRoute';
import Dictionary from '../pages/Dictionary';
import posed, { PoseGroup } from 'react-pose';

const propTypes = {
  setLoading: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
  initAuthFromExistingToken: PropTypes.func.isRequired
};

const RouteContainer = posed.div({
  enter: { opacity: 1, delay: 300, beforeChildren: true },
  exit: { opacity: 0 }
});

class App extends Component {
  componentDidMount () {
    this.props.initAuthFromExistingToken(() => this.props.setLoading(false));
  }

  render () {
    return (<Router>
      <Route render={({ location }) => (
        <PoseGroup>
          <RouteContainer key={location.key}>
            <Switch location={location}>
              <GuestRoute exact path="/" component={Welcome} />
              <GuestRoute path="/register" component={Register} />
              <GuestRoute path="/signin" component={SignIn} />
              <GuestRoute path="/forgot-password" component={ForgotPassword} />
              <GuestRoute path="/password/reset/:token" component={ResetPassword} />
              <AuthRoute path="/dictionary" component={Dictionary} />
              <AuthRoute path="/profile/:id" component={Profile} />
              <Route component={NotFound} />
            </Switch>
          </RouteContainer>
        </PoseGroup>
      )} />
    </Router>)
  }
}

App.propTypes = propTypes;

const mapDispatchToProps = {
  setLoading,
  initAuthFromExistingToken
};

const mapStateToProps = ({ loading }) => ({ loading });

export default connect(mapStateToProps, mapDispatchToProps)(App);
