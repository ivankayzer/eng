import React, {Component} from 'react';
import {Button, Card, Classes, H5, Icon} from '@blueprintjs/core';
import {Col} from 'react-grid-system';

class DictionaryCard extends Component {
  render () {
    return (
      <Col lg={4} md={6} sm={6}>
        <Card interactive={true}>
          <H5><a href="#">{ this.props.word }</a></H5>
          <span className="transcription">[{ this.props.words[0].transcription }]</span>
          <p>{ this.props.words[0].default_translation }</p>
          <Icon className="dictionary-sound" icon='volume-down' />
        </Card>
      </Col>
    );
  }
}

export default DictionaryCard;
