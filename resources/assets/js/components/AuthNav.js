import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logoutUser } from '../actions/auth';
import {Menu, MenuItem} from '@blueprintjs/core';

const propTypes = {
  auth: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

const navTypes = {
  navigate: PropTypes.func.isRequired,
  logout: PropTypes.func
};

class AuthNav extends Component {
  constructor (props) {
    super(props);
    this.state = {
      user: this.props.auth.user,
      hideMobileNav: true
    };
  }

  handleLogout = () => {
    this.props.logoutUser(() => this.props.history.push('/'));
  };

  navigate = (e) => {
    e.preventDefault();
    let link = e.target.parentElement.getAttribute('href');
    if (!link) {
      link = [...e.target.parentElement.childNodes].map(node => node.getAttribute('href')).filter(Boolean)[0];
    }
    this.props.history.push(link);
  };

  render () {
    return (
      <div>
        {this.props.auth.authenticated ? <UserRoutes logout={this.handleLogout} navigate={this.navigate} /> : <GuestRoutes navigate={this.navigate} />}
      </div>
    );
  }
}

const GuestRoutes = (props) => (
  <Menu>
    <MenuItem href='/signin' onClick={props.navigate} icon="log-in" text="Login" />
    <MenuItem href='/register' onClick={props.navigate} icon="user" text="Register" />
  </Menu>
);

const UserRoutes = (props) => (
  <Menu>
    <MenuItem href='/dictionary' onClick={props.navigate} icon="book" text="Dictionary" />
    <MenuItem href='/training' onClick={props.navigate} icon="predictive-analysis" text="Training" />
    <Menu.Divider />
    <MenuItem onClick={props.logout} icon="log-out" text="Logout" />
  </Menu>
);

AuthNav.propTypes = propTypes;
GuestRoutes.propTypes = navTypes;
UserRoutes.propTypes = navTypes;

const mapDispatchToProps = { logoutUser };
const mapStateToProps = ({ auth }) => ({ auth });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false }
)(withRouter(AuthNav));
