export const SET_DICTIONARY_LIST = 'SET_DICTIONARY_LIST';

export const setDictionaryList = data => ({
  type: SET_DICTIONARY_LIST,
  data
});

export const addWord = word => dispatch => {
  return window.axios.post('/api/dictionary', word).then(({ data: { data, meta } }) => {
    return Promise.resolve({ data, meta });
  }).catch(error => {
    return Promise.reject(error);
  });
};

export const getList = () => dispatch => {
  return window.axios.get('/api/dictionary').then(({ data: { data, meta } }) => {
    dispatch(setDictionaryList(data));
    return Promise.resolve({ data, meta });
  }).catch(error => {
    return Promise.reject(error);
  });
};
