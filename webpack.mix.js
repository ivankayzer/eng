let mix = require('laravel-mix');

mix.react('resources/assets/js/app.js', 'public/js')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .webpackConfig({
    externals: [
      'child_process'
    ],
    node: {
      fs: 'empty'
    }
  }).disableNotifications();
