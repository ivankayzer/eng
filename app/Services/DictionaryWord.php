<?php

namespace App\Services;

use Illuminate\Contracts\Support\Arrayable;

class DictionaryWord implements Arrayable
{
    private $text;
    private $type;
    private $transcription;
    private $translations;

    /**
     * DictionaryWord constructor.
     * @param $text
     * @param $type
     * @param $transcription
     * @param $translations
     */
    public function __construct($text, $type = '', $transcription = '', $translations = [])
    {
        $this->text = $text;
        $this->type = $type;
        $this->transcription = $transcription;
        $this->translations = $translations;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getTranscription()
    {
        return $this->transcription;
    }

    /**
     * @return mixed
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    public function defaultTranslation()
    {
        return data_get(collect($this->getTranslations())->first(), 'text');
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return [
            'text' => $this->getText(),
            'type' => $this->getType(),
            'transcription' => $this->getTranscription(),
            'translations' => $this->getTranslations(),
            'default_translation' => $this->defaultTranslation(),
        ];
    }

}
