<?php

namespace App\Services;

use App\DictionaryEntry as DictionaryEntryModel;
use App\DictionaryWord as DictionaryWordModel;
use Illuminate\Support\Facades\Auth;

class DictionaryEntry
{
    private $words;
    private $word;
    private $language;

    /**
     * DictionaryEntry constructor.
     * @param $json
     * @throws \Exception
     */
    public function __construct($json)
    {
        $this->words = collect(
            data_get(json_decode($json, true), 'en-' . Auth::user()->language . '.regular')
        )->map(function ($variant) {
            return new DictionaryWord(
                data_get($variant, 'text'),
                data_get($variant, 'pos.tooltip'),
                data_get($variant, 'ts'),
                $this->getTranslations(data_get($variant, 'tr'))
            );
        });

        $this->language = Auth::user()->language;

        if (!$this->words->count()) {
            throw new \Exception('This word does not exist');
        }

        $this->word = $this->words->first()->getText();
    }

    private function getTranslations($translations = [])
    {
        if (!$translations) {
            return [];
        }

        return collect($translations)->map(function ($translation) {
            return [
                'text' => data_get($translation, 'text'),
                'type' => data_get($translation, 'pos.tooltip'),
                'betterMatch' => collect(data_get($translation, 'mean'))->flatten()->toArray(),
                'examples' => collect(data_get($translation, 'ex'))
                    ->map(function ($example) {
                        return [
                            'original' => data_get($example, 'text'),
                            'translation' => data_get($example, 'tr.0.text')
                        ];
                    })->toArray(),
                'sentences' => collect(data_get($translation, 'exl'))->map(function ($sentence) {
                    return [
                        'original' => data_get($sentence, 'text'),
                        'translation' => data_get($sentence, 'tr.0.text')
                    ];
                })->toArray()
            ];
        })->toArray();
    }

    /**
     * @return static
     */
    public function getWords()
    {
        return $this->words;
    }

    /**
     * @return mixed
     */
    public function getWord()
    {
        return $this->word;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    public function save()
    {
        /** @var $model DictionaryEntryModel */
        $model = DictionaryEntryModel::create([
            'word' => $this->getWord(),
            'language' => $this->getLanguage()
        ]);

        $this->words->each(function ($word) use ($model) {
            /** @var $word DictionaryWordModel */
            $model->words()->create($word->toArray());
        });

        return $model->load('words');
    }
}
