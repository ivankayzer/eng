<?php

namespace App\Services;

use App\Http\Resources\DictionaryEntryResource;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class YandexTranslate
{
    private $link = 'https://dictionary.yandex.net/dicservice.json/lookupMultiple?ui=en&srv=tr-text&sid=b3f22020.5b86c25e.71796979&text={text}&dict=en-{lang}.regular&flags=103';
    /**
     * @var Client
     */
    private $http;

    /**
     * YandexTranslate constructor.
     * @param Client $http
     */
    public function __construct(Client $http)
    {
        $this->http = $http;
    }

    /**
     * @param $text
     * @return JsonResponse
     */
    public function translate($text)
    {
        $entry = new DictionaryEntry($this->http->get($this->prepareLink($text))->getBody()->getContents());

        $model = $entry->save();

        Auth::user()->dictionary()->attach($model->id);

        return new JsonResponse(['data' => new DictionaryEntryResource($model)]);
    }

    private function prepareLink($text)
    {
        $link = str_replace('{text}', $text, $this->link);
        $link = str_replace('{lang}', Auth::user()->language, $link);

        return $link;
    }
}
