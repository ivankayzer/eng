<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DictionaryEntry extends Model
{
    protected $guarded = [];

    public function words()
    {
        return $this->hasMany(DictionaryWord::class, 'entry_id', 'id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
