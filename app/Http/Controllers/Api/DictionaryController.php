<?php

namespace App\Http\Controllers\Api;

use App\DictionaryEntry;
use App\Http\Controllers\Controller;
use App\Http\Resources\DictionaryEntryCollection;
use App\Http\Resources\DictionaryEntryResource;
use App\Services\YandexTranslate;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DictionaryController extends Controller
{
    /**
     * @var YandexTranslate
     */
    private $translator;

    /**
     * DictionaryController constructor.
     * @param YandexTranslate $translator
     */
    public function __construct(YandexTranslate $translator)
    {
        $this->translator = $translator;
    }

    public function index(Request $request)
    {
        return new JsonResponse([
            'data' => new DictionaryEntryCollection($request->user()->dictionary()->with('words')->get())
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'text' => 'required'
        ]);

        $word = DictionaryEntry::where('word', $request->text)->where('language', Auth::user()->language)->first();

        if (!$word) {
            return $this->translator->translate($request->text);
        }

        if ($request->user()->hasInDictionary($word->word)) {
            return new JsonResponse([
                'type' => 'error',
                'message' => 'You already have this word in your dictionary'
            ]);
        }

        $request->user()->dictionary()->attach($word->id);

        return new JsonResponse([
            'data' => new DictionaryEntryResource($word)
        ]);
    }
}
