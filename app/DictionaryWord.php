<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DictionaryWord extends Model
{
    protected $guarded = ['entry_id'];

    public $timestamps = false;

    protected $casts = [
        'translations' => 'array'
    ];
}
